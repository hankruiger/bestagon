/// Integer division and round towards -inf
/// (Could be identical to `isize::div_euclid`?)
pub trait DivFloor {
  fn my_div_floor(self, rhs: Self) -> Self;
}

/// Integer division and round towards +inf
pub trait DivCeil {
  fn my_div_ceil(self, rhs: Self) -> Self;
}

impl DivFloor for isize {
  fn my_div_floor(self, rhs: isize) -> isize {
    if self < 0 && self % rhs != 0 {
      self / rhs - 1
    } else {
      self / rhs
    }
  }
}

impl DivCeil for isize {
  fn my_div_ceil(self, rhs: isize) -> isize {
    if self > 0 && self % rhs != 0 {
      self / rhs + 1
    } else {
      self / rhs
    }
  }
}

// TODO: remove this module when https://github.com/rust-lang/rust/issues/88581 lands

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test_cases_floor() {
    assert_eq!((-4).my_div_floor(2), -2);
    assert_eq!((-3).my_div_floor(2), -2);
    assert_eq!((-2).my_div_floor(2), -1);
    assert_eq!((-1).my_div_floor(2), -1);
    assert_eq!(0.my_div_floor(2), 0);
    assert_eq!(1.my_div_floor(2), 0);
    assert_eq!(2.my_div_floor(2), 1);
    assert_eq!(3.my_div_floor(2), 1);
    assert_eq!(4.my_div_floor(2), 2);

    assert_eq!((-7).my_div_floor(3), -3);
    assert_eq!((-6).my_div_floor(3), -2);
    assert_eq!((-5).my_div_floor(3), -2);
    assert_eq!((-4).my_div_floor(3), -2);
    assert_eq!((-3).my_div_floor(3), -1);
    assert_eq!((-2).my_div_floor(3), -1);
    assert_eq!((-1).my_div_floor(3), -1);

    assert_eq!(0.my_div_floor(3), 0);
    assert_eq!(1.my_div_floor(3), 0);
    assert_eq!(2.my_div_floor(3), 0);
    assert_eq!(3.my_div_floor(3), 1);
    assert_eq!(4.my_div_floor(3), 1);
    assert_eq!(5.my_div_floor(3), 1);
    assert_eq!(6.my_div_floor(3), 2);
    assert_eq!(7.my_div_floor(3), 2);
  }

  #[test]
  fn test_cases_ceil() {
    assert_eq!((-4).my_div_ceil(2), -2);
    assert_eq!((-3).my_div_ceil(2), -1);
    assert_eq!((-2).my_div_ceil(2), -1);
    assert_eq!((-1).my_div_ceil(2), 0);
    assert_eq!(0.my_div_ceil(2), 0);
    assert_eq!(1.my_div_ceil(2), 1);
    assert_eq!(2.my_div_ceil(2), 1);
    assert_eq!(3.my_div_ceil(2), 2);
    assert_eq!(4.my_div_ceil(2), 2);

    assert_eq!((-7).my_div_ceil(3), -2);
    assert_eq!((-6).my_div_ceil(3), -2);
    assert_eq!((-5).my_div_ceil(3), -1);
    assert_eq!((-4).my_div_ceil(3), -1);
    assert_eq!((-3).my_div_ceil(3), -1);
    assert_eq!((-2).my_div_ceil(3), 0);
    assert_eq!((-1).my_div_ceil(3), 0);
    assert_eq!(0.my_div_ceil(3), 0);
    assert_eq!(1.my_div_ceil(3), 1);
    assert_eq!(2.my_div_ceil(3), 1);
    assert_eq!(3.my_div_ceil(3), 1);
    assert_eq!(4.my_div_ceil(3), 2);
    assert_eq!(5.my_div_ceil(3), 2);
    assert_eq!(6.my_div_ceil(3), 2);
    assert_eq!(7.my_div_ceil(3), 3);
  }
}
