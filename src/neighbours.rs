// Implementing this trait will give you the `Neighbours` trait, giving an
// iterator over the neighbours.
pub trait Neighbour: Sized {
  fn get_neighbour(&self, n: u8) -> Option<Self>;
}

pub trait Neighbours: Neighbour {
  fn neighbours(&self) -> NeighbourIterator<Self> {
    NeighbourIterator {
      coord: self,
      count: 0,
    }
  }
}

pub struct NeighbourIterator<'a, C> {
  coord: &'a C,
  count: u8,
}

impl<C> Iterator for NeighbourIterator<'_, C>
where
  C: Neighbour,
{
  type Item = C;

  fn next(&mut self) -> Option<C> {
    self.count += 1;
    self.coord.get_neighbour(self.count - 1)
  }
}

// Marks all structs that implement the `Neighbour` trait to use the default
// implementation of the `Neighbours` trait. (Why isn't this inferred?)
impl<C> Neighbours for C where C: Neighbour {}
