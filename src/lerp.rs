use crate::{HexAx, HexCb};

pub trait LinearInterpolation {
  fn lerp(&self, to: &Self, t: f32) -> Self;
}

impl LinearInterpolation for f32 {
  fn lerp(&self, to: &f32, t: f32) -> f32 {
    self + (to - self) * t
  }
}

impl LinearInterpolation for HexCb {
  /// ```
  /// use bestagon::{HexCb, lerp::LinearInterpolation};
  /// assert_eq!(HexCb::new(0, 0, 0).lerp(&HexCb::new(5, -4, -1), 0.00), HexCb::new(0, 0, 0));
  /// assert_eq!(HexCb::new(0, 0, 0).lerp(&HexCb::new(5, -4, -1), 0.25), HexCb::new(1, -1, 0));
  /// assert_eq!(HexCb::new(0, 0, 0).lerp(&HexCb::new(5, -4, -1), 0.49), HexCb::new(2, -2, 0));
  /// assert_eq!(HexCb::new(0, 0, 0).lerp(&HexCb::new(5, -4, -1), 0.51), HexCb::new(3, -2, -1));
  /// assert_eq!(HexCb::new(0, 0, 0).lerp(&HexCb::new(5, -4, -1), 0.75), HexCb::new(4, -3, -1));
  /// assert_eq!(HexCb::new(0, 0, 0).lerp(&HexCb::new(5, -4, -1), 1.00), HexCb::new(5, -4, -1));
  /// ```
  fn lerp(&self, to: &HexCb, t: f32) -> HexCb {
    let x = (self.q as f32).lerp(&(to.q as f32), t).round() as isize;
    let y = (self.r as f32).lerp(&(to.r as f32), t).round() as isize;
    HexCb::new(x, y, -x - y)
  }
}

impl LinearInterpolation for HexAx {
  /// ```
  /// use bestagon::{HexAx, lerp::LinearInterpolation};
  /// assert_eq!(HexAx::new(0, 0).lerp(&HexAx::new(5, -4), 0.00), HexAx::new(0, 0));
  /// assert_eq!(HexAx::new(0, 0).lerp(&HexAx::new(5, -4), 0.25), HexAx::new(1, -1));
  /// assert_eq!(HexAx::new(0, 0).lerp(&HexAx::new(5, -4), 0.49), HexAx::new(2, -2));
  /// assert_eq!(HexAx::new(0, 0).lerp(&HexAx::new(5, -4), 0.51), HexAx::new(3, -2));
  /// assert_eq!(HexAx::new(0, 0).lerp(&HexAx::new(5, -4), 0.75), HexAx::new(4, -3));
  /// assert_eq!(HexAx::new(0, 0).lerp(&HexAx::new(5, -4), 1.00), HexAx::new(5, -4));
  /// ```
  fn lerp(&self, to: &HexAx, t: f32) -> HexAx {
    let x = (self.q as f32).lerp(&(to.q as f32), t).round() as isize;
    let y = (self.r as f32).lerp(&(to.r as f32), t).round() as isize;
    HexAx::new(x, y)
  }
}
