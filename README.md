# `bestagon`

An experimental engine for discrete stuff in a hexagonal grid.

It is based mostly on [this excellent web page](https://www.redblobgames.com/grids/hexagons/), and the name is inspired by [this excellent video](https://youtu.be/thOifuHs6eY).

It is meant to be used as a back-end for game engine logic.
